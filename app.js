const express = require('express') 
//dans le container express n'est pas 
// disponible il requiert une installation, donc on va voir dans le hub
// s'il n'y a pas une image qui contient express et si ca n'existe pas
// je peux la créer moi même.
const app = express()

app.get('/', function (req, res) {
    res.send('Hello World')
  })

  app.get('/help', function (req, res) {
    res.send('A l\'aide !!!')
  })

app.listen(3000) //ici ca tourne en boucle infini mais le container créé
// ne tourne pas en boucle infini car ce n'est qu'un intermédiaire.